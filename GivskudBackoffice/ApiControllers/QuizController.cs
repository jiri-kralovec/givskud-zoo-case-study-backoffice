﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Collections.Generic;

using Umbraco.Web;
using Umbraco.Web.WebApi;

using Newtonsoft.Json;
using GivskudBackoffice.ApiServices;
using GivskudBackoffice.ApiModels;
using Umbraco.Core.Models.PublishedContent;

namespace GivskudBackoffice.ApiControllers
{
    public class QuizController : UmbracoApiController
    {
        [WebApi.OutputCache.V2.CacheOutput(ClientTimeSpan = 0, ServerTimeSpan = 0)]
        [HttpGet]
        public HttpResponseMessage Get()
        {

            JsonResponse Response = new JsonResponse(true, new SecureContext());
            Response.Create();

            if (Response.IsAuthentic == true || Response.RequiresAuth == false)
            {

                Int32.TryParse(HttpContext.Current.Request.QueryString["id"], out int QuizId);

                List<Quiz> DataContainer = new List<Quiz>();

                IEnumerable<IPublishedContent> DataSource;

                if(QuizId == 0)
                {
                    DataSource = Umbraco.Content(1127).Children().Where(x => x.IsDocumentType("quizPost")).Where(x => x.IsVisible());
                } else
                {
                    DataSource = Umbraco.Content(1127).Children().Where(x => x.IsDocumentType("quizPost")).Where(x => x.IsVisible() && x.Id == QuizId);
                }

                if(DataSource.Count() > 0) { 
                    
                    foreach (var Node in DataSource)
                    {

                        var ImageObject = (IPublishedContent)Node.GetProperty("image").GetValue();
                        string PostImage = "https://" + HttpContext.Current.Request.Url.Host + ImageObject.Url;

                        Quiz QuizInstance = new Quiz
                        {
                            ID = Node.Id,
                            Title = Node.GetProperty("title").GetValue().ToString(),
                            Image = "https://www.jirikralovec.cz/dev/givskud/images/demo-animalimage.jpg",
                            IsLockedByDefault = (bool)Node.GetProperty("isLockedByDefault").GetValue(),
                            Questions = new List<QuizQuestion>()
                        };

                        List<IPublishedElement> QuizQuestions = (List<IPublishedElement>)Node.GetProperty("questions").GetValue();

                        if(QuizQuestions.Count() > 0)
                        {
                            foreach(var QuestionNode in QuizQuestions)
                            {
                                QuizQuestion QuestionInstance = new QuizQuestion {
                                    Question = QuestionNode.GetProperty("questionText").GetValue().ToString(),
                                    Answers = new Dictionary<int, string>()
                                };

                                Int32.TryParse(QuestionNode.GetProperty("correctAnswer").GetValue().ToString(), out int CorrectAnswer);
                                QuestionInstance.CorrectAnswer = CorrectAnswer;

                                string[] AnswersDataspace = (string[])QuestionNode.GetProperty("answers").GetValue();
                                if(AnswersDataspace.Count() > 0)
                                {
                                    for(var i = 0; i < AnswersDataspace.Count(); i++)
                                    {
                                        QuestionInstance.Answers.Add(i, AnswersDataspace[i]);
                                    }
                                }

                                QuizInstance.Questions.Add(QuestionInstance);
                            }
                        }

                        DataContainer.Add(QuizInstance);

                    }

                }

                Response.Set(new StringContent(JsonConvert.SerializeObject(DataContainer), ApiContext.GetEncoding(), ApiContext.GetOutputType()));

            }
            
            return Response.Get();

        }
    }
}