﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Collections.Generic;

using Umbraco.Web;
using Umbraco.Web.WebApi;

using Newtonsoft.Json;
using GivskudBackoffice.ApiServices;
using GivskudBackoffice.ApiModels;

namespace GivskudBackoffice.ApiControllers {
    public class EventsController : UmbracoApiController {
        [HttpGet]
        public HttpResponseMessage Get() {

            JsonResponse Response = new JsonResponse(true, new SecureContext());

            if(Response.IsAuthentic == true || Response.RequiresAuth == false) {

                List<Event> DataContainer = new List<Event>();
                var DataSource = Umbraco.Content(1085).Children().Where(x => x.IsDocumentType("eventPost")).Where(x => x.IsVisible());

                if(DataSource.Count() > 0) {
                    foreach(var Node in DataSource) {
                        DataContainer.Add(new Event {
                            ID = Node.Id.ToString(),
                            Title = Node.GetProperty("title").GetValue().ToString(),
                            Desc = Node.GetProperty("description").GetValue().ToString(),
                            IsBoundToDate = (bool)Node.GetProperty("isDateOnly").GetValue(),
                            EventDate = Convert.ToDateTime(Node.GetProperty("eventDate").GetValue().ToString()),
                            EventTime = Convert.ToDateTime(Node.GetProperty("eventTime").GetValue().ToString()),
                        });
                    }
                }

                DataContainer = DataContainer.OrderByDescending(x => x.EventDate).ThenBy(x => x.EventTime).ToList();
                
                Response.Create();
                Response.Set(new StringContent(JsonConvert.SerializeObject(DataContainer), ApiContext.GetEncoding(), ApiContext.GetOutputType()));
            } else {
                Response.Create();
            }

            return Response.Get();

        }
    }
}