﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Collections.Generic;

using Umbraco.Web;
using Umbraco.Web.WebApi;

using Newtonsoft.Json;
using GivskudBackoffice.ApiServices;
using GivskudBackoffice.ApiModels;
using Umbraco.Core.Models.PublishedContent;

namespace GivskudBackoffice.ApiControllers {
    public class NewsController : UmbracoApiController {
        [WebApi.OutputCache.V2.CacheOutput(ClientTimeSpan = 0, ServerTimeSpan = 0)]
        [HttpGet]
        public HttpResponseMessage Get() {

            JsonResponse Response = new JsonResponse(true, new SecureContext());

            if(Response.IsAuthentic == true || Response.RequiresAuth == false) {

                List<Post> DataContainer = new List<Post>();
                var DataSource = Umbraco.Content(1084).Children().Where(x => x.IsDocumentType("newsPost")).Where(x => x.IsVisible());
                foreach(var Node in DataSource) {

                    var ImageObject = (IPublishedContent)Node.GetProperty("image").GetValue();

                    if (DataSource.Count() > 0) {

                        string PostImage = "https://" + HttpContext.Current.Request.Url.Host + ImageObject.Url;

                        DataContainer.Add(new Post {
                            ID = Node.Id.ToString(),
                            Title = Node.GetProperty("title").GetValue().ToString(),
                            Body = Node.GetProperty("body").GetValue().ToString(),
                            Image = "https://www.jirikralovec.cz/dev/givskud/images/demo-animalimage.jpg",
                            PublishedOn = Node.CreateDate,
                            PublishedBy = Node.CreatorName.ToString()
                        });
                    }

                }

                DataContainer = DataContainer.OrderByDescending(x => x.PublishedOn).ToList();

                Response.Create();
                Response.Set(new StringContent(JsonConvert.SerializeObject(DataContainer), ApiContext.GetEncoding(), ApiContext.GetOutputType()));
            } else {
                Response.Create();
            }

            return Response.Get();

        }
    }
}