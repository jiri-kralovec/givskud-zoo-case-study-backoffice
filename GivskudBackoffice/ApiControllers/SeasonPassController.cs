﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Collections.Generic;

using Umbraco.Web;
using Umbraco.Web.WebApi;

using Newtonsoft.Json;
using GivskudBackoffice.ApiServices;
using GivskudBackoffice.EncryptionService;
using GivskudBackoffice.ApiModels;  

namespace GivskudBackoffice.ApiControllers {
    public class SeasonPassController : UmbracoApiController {

        private readonly int pageid = 1083;
        
        [HttpGet]
        public HttpResponseMessage Get() {

            SecureContext Secure = new SecureContext();
            string PassID = Secure.GetHeaderParam("PassID");

            JsonResponse Response = new JsonResponse(true, new SecureContext());

            if(Response.IsAuthentic == true || Response.RequiresAuth == false) {

                List<SeasonPass> DataContainer = new List<SeasonPass>();
                var DataSource = Umbraco.Content(pageid).Children();

                if (DataSource.Count() > 0) {
                    foreach (var Node in DataSource) {
                        if(Node.GetProperty("passID").GetValue().ToString() != PassID) {
                            continue;
                        }
                        DataContainer.Add(new SeasonPass {
                            ID = "n/a",
                            Holder = Node.GetProperty("holderName").GetValue().ToString(),
                            ValidFrom = Convert.ToDateTime(Node.GetProperty("validFrom").GetValue().ToString()).ToString("dd-MM-yyyy"),
                            ValidTo = Convert.ToDateTime(Node.GetProperty("validTo").GetValue().ToString()).ToString("dd-MM-yyyy"),
                            AcquiredOn = Convert.ToDateTime(Node.GetProperty("acquiredOn").GetValue().ToString()).ToString("dd-MM-yyyy")
                        });
                    }
                }

                Response.Create();
                Response.Set(new StringContent(JsonConvert.SerializeObject(DataContainer), ApiContext.GetEncoding(), ApiContext.GetOutputType()));
            } else {
                Response.Create();
            }

            return Response.Get();

        }
        
        [HttpPost]
        public HttpResponseMessage Post([FromBody] SeasonPass Data) {

            JsonResponse Response = new JsonResponse(true, new SecureContext());
            Response.Create();

            if(Response.IsAuthentic) {

                var CS = Services.ContentService;
                var PassInstance = CS.Create(EncDecService.Hash(Data.ID), pageid, "seasonPass");
                    PassInstance.SetValue("passID", EncDecService.Hash(Data.ID));
                    PassInstance.SetValue("holderName",EncDecService.Encrypt(Data.Holder));
                    PassInstance.SetValue("validFrom", Convert.ToDateTime(Data.ValidFrom));
                    PassInstance.SetValue("validTo", Convert.ToDateTime(Data.ValidTo));
                    PassInstance.SetValue("acquiredOn", Convert.ToDateTime(Data.AcquiredOn));
                string ResponseMsg;

                var IsSaved = CS.SaveAndPublish(PassInstance);

                if(IsSaved.Success) {
                    ResponseMsg = "Content has been saved";
                } else {
                    ResponseMsg = "Error while saving";
                }

                Response.Set(new StringContent(ResponseMsg, ApiContext.GetEncoding(), "text/plain"));

            }
            
            return Response.Get();
            
        }
    }
}