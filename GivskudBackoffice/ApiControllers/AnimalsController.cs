﻿using System;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Collections.Generic;

using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.WebApi;

using Umbraco.Web.Composing;

using Newtonsoft.Json;
using GivskudBackoffice.ApiServices;
using GivskudBackoffice.ApiModels;
using Umbraco.Core.Models.PublishedContent;

namespace GivskudBackoffice.ApiControllers {
    public class AnimalsController : UmbracoApiController {
        [HttpGet]
        public HttpResponseMessage Get() {

            JsonResponse Response = new JsonResponse(true, new SecureContext());

            if(Response.IsAuthentic == true || Response.RequiresAuth == false) {

                List<Animal> DataContainer = new List<Animal>();
                IEnumerable<IPublishedContent> DataSource = Umbraco.Content(1082).Children().Where(x => x.IsDocumentType("animalPost")).Where(x => x.IsVisible());
                Int32.TryParse(HttpContext.Current.Request.QueryString["sortbyarea"], out int SortParameter);

                if (DataSource.Count() > 0) {

                    foreach (var Node in DataSource) {

                        /* Sorting by area */
                        int AreaID = AnimalsControllerHelpers.GetNestedPropertyId(Node, "zooArea");
                        int QuizID = AnimalsControllerHelpers.GetNestedPropertyId(Node, "quizPost");
                        
                        if (SortParameter != 0 && SortParameter != AreaID)
                        {
                            continue;
                        }

                        /* Sorting by area */

                        var IconObject = (IPublishedContent)Node.GetProperty("animalIcon").GetValue();
                        var ImageObject = (IPublishedContent)Node.GetProperty("animalImage").GetValue();

                        string IconUrl = "https://" + HttpContext.Current.Request.Url.Host + IconObject.Url;
                        string ImageUrl = "https://" + HttpContext.Current.Request.Url.Host + ImageObject.Url;

                        Animal ResponseObject = new Animal {
                            ID      = Node.Id,
                            Icon    = "https://www.jirikralovec.cz/dev/givskud/images/demo-animalicon.png",
                            Image   = "https://www.jirikralovec.cz/dev/givskud/images/demo-animalimage.jpg",
                            Name    = Node.GetProperty("animalName").GetValue().ToString(),
                            Content = AnimalsControllerHelpers.FormatGroupedStringContent((string[])Node.GetProperty("animalContent").GetValue()),

                            Height  = AnimalsControllerHelpers.GetGroupedStringContent((string[])Node.GetProperty("animalHeight").GetValue(), " - ") + " cm",
                            Length  = AnimalsControllerHelpers.GetGroupedStringContent((string[])Node.GetProperty("animalLength").GetValue(), " - ") + " cm",
                            Weight  = AnimalsControllerHelpers.GetGroupedStringContent((string[])Node.GetProperty("animalWeight").GetValue(), " - ") + " kg",

                            PregnancyTime   = Node.GetProperty("animalPregnancyTime").GetValue().ToString(),
                            Descendants     = AnimalsControllerHelpers.GetGroupedStringContent((string[])Node.GetProperty("animalDescendants").GetValue(), " - "),
                            Lifetime        = AnimalsControllerHelpers.GetGroupedStringContent((string[])Node.GetProperty("animalLifetime").GetValue(), " - "),

                            Continent   = AnimalsControllerHelpers.GetGroupedStringContent((string[])Node.GetProperty("animalContinent").GetValue(), ", "),
                            Species     = Node.GetProperty("animalSpecies").GetValue().ToString(),
                            Eats        = Node.GetProperty("animalEats").GetValue().ToString(),
                            Status      = Node.GetProperty("animalStatus").GetValue().ToString(),

                            QuizID = -1,
                            AreaID = -1
                        };

                        DataContainer.Add(ResponseObject);
                    }
                }

                DataContainer = DataContainer.OrderBy(x => x.Name).ToList();

                Response.Create();
                Response.Set(new StringContent(JsonConvert.SerializeObject(DataContainer), ApiContext.GetEncoding(), ApiContext.GetOutputType()));
            } else {
                Response.Create();
            }

            return Response.Get();

        }
        [HttpGet]
        public Dictionary<int,string> Demo()
        {

            Dictionary<int, string> output = new Dictionary<int, string>();

            IEnumerable<IPublishedContent> DataSource = Umbraco.Content(1082).Children().Where(x => x.IsDocumentType("animalPost")).Where(x => x.IsVisible());

            // Current.UmbracoHelper.Content(1082)
                

            foreach(var Node in DataSource)
            {

                var outtext = "no quiz";
                var test = Node.Value("quizID");
                
                if(test != null)
                {
                    
                    outtext = test.ToString();
                }

                output.Add(Node.Id, outtext);

            }

            /*
             * Nodes containing quiz:
             *  1155
             *  1154
             *  1153
             * */

            return output;
            
        }
    }
    public class AnimalsControllerHelpers {
        public static int GetNestedPropertyId(IPublishedContent Node, string DocumentType) {

            IPublishedContent ChildNode = Node.Children.Where(x => x.IsDocumentType(DocumentType)).FirstOrDefault();

            return ChildNode == null ? 0 : 1;
            
        }
        public static List<string> FormatGroupedStringContent(string[] GroupedInput) {
            List<string> GroupedResponse = new List<string>();
        
            foreach(string Line in GroupedInput) {
                GroupedResponse.Add(Line);
            }

            return GroupedResponse;
        }
        public static string GetGroupedStringContent(string[] GroupedInput, string delimiter = "") {

            string Response = "";
           
            for(int i = 0; i < GroupedInput.Length; i++) {
                Response += GroupedInput[i];
                if(i < GroupedInput.Length - 1) {
                    Response += delimiter;
                }
            }

            return Response;

        }
    }
}